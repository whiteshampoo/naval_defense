# +------------------------------------------------------+
# |                                                      |
# | Written by Benedikt Wicklein aka whiteshampoo - 2022 | 
# |                                                      |
# |               License: CC BY-NC-SA 4.0               |
# |  https://creativecommons.org/licenses/by-nc-sa/4.0/  |
# |                                                      |
# +------------------------------------------------------+


extends Line2D

#-[CONSTANTS]---------------------------------------------------------------------

const SHIPS : Array = [
	preload("res://Objects/Units/Ships/ShipNormal.tscn"),
	preload("res://Objects/Units/Ships/ShipFast.tscn"),
	preload("res://Objects/Units/Ships/ShipStrong.tscn"),
	preload("res://Objects/Units/Ships/ShipWarrior.tscn"),
	preload("res://Objects/Units/Ships/ShipPirate.tscn"),
]

#-[SIGNALS]---------------------------------------------------------------------



#-[EXPORT]----------------------------------------------------------------------

export (Array, Resource) var patterns = Array()
export var autorun : bool = true
export var endless : bool = false

#-[ONREADY]---------------------------------------------------------------------

onready var Wait : Timer = $Wait
onready var Overlay : CanvasLayer = get_node("../Overlay")
onready var ScrollController : Node2D = get_node("../Camera2D/ScrollController")
onready var CursorController : Node2D = get_node("../CursorController")

#-[VAR]-------------------------------------------------------------------------

var running : bool = false
var pattern : Pattern = null
var pattern_num : int = 0
var pattern_pos : int = 0
var wave_finished : bool = false
var Fishing : ColorRect = null
var failed : bool = false
var ships : Array = Array()
var endless_level : int = 0

#-[SETGET METHODS]--------------------------------------------------------------



#-[BUILTIN GODOT METHODS]-------------------------------------------------------

func _ready() -> void:
	assert(patterns, "No Pattern found")
	if autorun:
		play()

#-[INHERITED METHODS]-----------------------------------------------------------



#-[OWN METHODS]-----------------------------------------------------------------

func play() -> void:
	if endless and pattern:
		pattern = Pattern.new()
		pattern.wait_time = max(1.0, 3.0 - endless_level * 0.2)
		pattern.ships.clear()
		for x in 10 + endless_level * 2:
			pattern.ships.append(SHIPS[randi() % int(min(sqrt(endless_level * 1.5) * 1.3 + 1.0, SHIPS.size()))])
		endless_level += 1
		patterns[0] = pattern
		pattern_num = 0
	if pattern_num >= patterns.size():
		WishTransition.change_scene("res://Menu/Menu.tscn")
		return

	pattern = patterns[pattern_num]
	pattern_num += 1
	pattern_pos = 0
	Wait.wait_time = pattern.wait_time
	call_deferred("execute_pattern")


func execute_pattern() -> void:
	if not pattern.ships:
		add_dialog(pattern.dialog_at_win, true)
		return
	if not pattern.ships[pattern_pos]:
		push_warning("Ship-pattern %d/%d is null" % [pattern_num - 1, pattern_pos])
	var ship : Area2D = pattern.ships[pattern_pos].instance()
	ships.append(ship)
	ship.connect("sunk", self, "_remove_ship") # warning-ignore:return_value_discarded
	ship.connect("exploding", self, "_ship_exploded") # warning-ignore:return_value_discarded
	pattern_pos += 1
	ship.global_position = global_position +  points[0] + (points[1] - points[0]) * randf()
	get_parent().add_child(ship)
	if pattern_pos < pattern.ships.size():
		Wait.start()
	else:
		wave_finished = true


func add_dialog(timeline : String, win : bool) -> void:
	if not timeline: 
		if win:
			_dialog_win_end(timeline)
		else:
			_dialog_fail_end(timeline)
		return
	var dialog : CanvasLayer = Dialogic.start(timeline, '', "res://addons/dialogic/Nodes/DialogNode.tscn", false, true)
	add_child(dialog)
	dialog.connect("timeline_end", self, "_dialog_win_end" if win else "_dialog_fail_end") # warning-ignore:return_value_discarded
	#dialog.connect("dialogic_signal", self, "_dialogic_signal") # warning-ignore:return_value_discarded
	Overlay.kill_fish()
	ScrollController.can_scroll = false
	CursorController.can_build = false

#-[SIGNAL METHODS]--------------------------------------------------------------

func _on_Timer_timeout() -> void:
	call_deferred("execute_pattern")


func _dialog_win_end(_timeline : String) -> void:
	if pattern.tech_level_inc:
		get_parent().tech_level += 1
	Overlay.mod_gold(pattern.add_money)
	play()
	ScrollController.can_scroll = true
	CursorController.can_build = true



func _dialog_fail_end(_timeline : String) -> void:
	WishTransition.change_scene("res://Menu/Menu.tscn")
	ScrollController.can_scroll = true
	CursorController.can_build = true


func _remove_ship(ship : Area2D) -> void:
	assert(ship in ships)
	ships.erase(ship)
	if failed: return
	if not ships and wave_finished:
#		if not pattern.dialog_at_win:
#			play()
#			return
		add_dialog(pattern.dialog_at_win, true)


func _ship_exploded() -> void:
	if failed: return
	Overlay.mod_lives(-1)
	if Overlay.lives <= 0:
		failed = true
		if not pattern.dialog_at_fail:
			pattern.dialog_at_fail = "Fail"
		add_dialog(pattern.dialog_at_fail, false)


func _dialogic_signal(s : String) -> void: # no longer needed
	match s:
		"tech":
			get_parent().tech_level += 1
		_:
			assert(false, "Unknown dialogic signal '%s'" % s)

# +------------------------------------------------------+
# |                                                      |
# | Written by Benedikt Wicklein aka whiteshampoo - 2022 | 
# |                                                      |
# |               License: CC BY-NC-SA 4.0               |
# |  https://creativecommons.org/licenses/by-nc-sa/4.0/  |
# |                                                      |
# +------------------------------------------------------+


extends Node2D

#-[CONSTANTS]---------------------------------------------------------------------



#-[SIGNALS]---------------------------------------------------------------------



#-[EXPORT]----------------------------------------------------------------------

export var tech_level : int = 10 setget set_tech_level

#-[ONREADY]---------------------------------------------------------------------

onready var ScrollController : Node2D = $Camera2D/ScrollController
onready var Cam : Camera2D = $Camera2D

#-[VAR]-------------------------------------------------------------------------

var grouping_init : bool = false

#-[SETGET METHODS]--------------------------------------------------------------

func set_tech_level(level : int) -> void:
	tech_level = level
	if grouping_init:
		call_deferred("grouping")
		

#-[BUILTIN GODOT METHODS]-------------------------------------------------------

func _ready() -> void:
	set_tech_level(tech_level)
	var rect : Rect2 = $MapSand.get_used_rect()
	rect.position.x = min(rect.position.x, 0)
	rect.position.y = min(rect.position.y, 0)
	rect.size.x = max(rect.size.x, 40)
	rect.size.y = max(rect.size.y, 24)
	Cam.limit_left = int((rect.position.x - 2.0) * 32.0)
	Cam.limit_right = int((rect.end.x + 2.0) * 32.0)
	Cam.limit_top = int((rect.position.y - 2.0) * 32.0)
	Cam.limit_bottom = int((rect.end.y + 2.0) * 32.0)
	grouping()


func _process(_delta : float) -> void:
	if Input.is_action_pressed("ui_left"):
		tech_level += 1
	if Input.is_action_just_pressed("ui_cancel"):
		add_child(preload("res://Wish/WishMenu/Ingame/IngameMenu.tscn").instance())

#-[INHERITED METHODS]-----------------------------------------------------------



#-[OWN METHODS]-----------------------------------------------------------------

func scrolling(enabled : bool) -> void:
	ScrollController.can_scroll = enabled


func grouping() -> void:
	get_tree().call_group("BuildButton", "test_tech")
	grouping_init = true

#-[SIGNAL METHODS]--------------------------------------------------------------

# +------------------------------------------------------+
# |                                                      |
# | Written by Benedikt Wicklein aka whiteshampoo - 2022 | 
# |                                                      |
# |                     License: CC0                     |
# |  https://creativecommons.org/publicdomain/zero/1.0/  |
# |                                                      |
# +------------------------------------------------------+

extends VBoxContainer
tool

export var blacklist : bool = true
export var list : PoolStringArray = ["Master"]
export var volume_scene : PackedScene = preload("BusControl.tscn")
export var reset : bool = false setget set_reset
export var update : bool = false setget set_update

var children : Array = Array()

func set_reset(_r : bool) -> void:
	clear_menu()
	create_menu()

func set_update(_u : bool) -> void:
	for bus in AudioServer.bus_count:
		var found : bool = false
		var bus_name : String = AudioServer.get_bus_name(bus)
		for child in children:
			if child.bus == bus_name:
				found = true
				break
		if found:
			continue
		if check_list(bus_name):
			add_bus(bus_name)

func _ready() -> void:
	for child in get_children():
		if child.has_meta("BusesAdded"):
			children.append(child)


func add_bus(bus_name : String) -> void:
	var bus_control : WishBusControl = volume_scene.instance()
	add_child(bus_control)
	bus_control.label = bus_name
	bus_control.bus = bus_name
	bus_control.set_deferred("name", bus_name)
	bus_control.set_meta("BusesAdded", true)
	
	bus_control.owner = self
	children.append(bus_control)
	get_viewport().get_texture().get_data().save_png("res://test.png")

func check_list(bus_name : String) -> bool:
	if blacklist and bus_name in list:
		return false
	if not blacklist and not bus_name in list:
		return false
	return true


func clear_menu() -> void:
	for child in children:
		child.queue_free()
	children.clear()


func create_menu() -> void:
	if children:
		push_warning("Children were still present.")
		clear_menu()

	for bus in AudioServer.bus_count:
		var bus_name : String = AudioServer.get_bus_name(bus)
		if check_list(bus_name):
			add_bus(bus_name)

# +------------------------------------------------------+
# |                                                      |
# | Written by Benedikt Wicklein aka whiteshampoo - 2022 | 
# |                                                      |
# |                     License: CC0                     |
# |  https://creativecommons.org/publicdomain/zero/1.0/  |
# |                                                      |
# +------------------------------------------------------+

extends Control

onready var LanguageOptions : OptionButton = $Container/LanguageOptions
onready var FactoryResetButton : Button = $Container/FactoryResetButton

var language_names : Array = Array()

func _ready() -> void:
	var languages : Dictionary = WishTranslation.languages
	var selected_language : String = WishSettings.get_value("language")
	var idx : int = 0
	for key in languages:
		language_names.append(key)
		LanguageOptions.add_item(languages[key])
		if selected_language == key:
			LanguageOptions.select(idx)
		idx += 1


func _on_FactoryResetCheck_toggled(button_pressed: bool) -> void:
	FactoryResetButton.disabled = not button_pressed


func _on_FactoryResetButton_pressed() -> void:
	WishTransition.clear_stack()
	if OS.get_name() == "HTML5":
# warning-ignore:return_value_discarded
		get_tree().change_scene("res://Wish/WishMenu/Sub/General/HTMLReload.tscn")
	WishSettings.factory_reset()
	get_tree().call_deferred("quit")


func _on_LanguageOptions_item_selected(index: int) -> void:
	var new_language : String = language_names[index]
	WishSettings.set_value("language", new_language)
	WishTranslation.set_translation(new_language)

# +------------------------------------------------------+
# |                                                      |
# | Written by Benedikt Wicklein aka whiteshampoo - 2022 | 
# |                                                      |
# |                     License: CC0                     |
# |  https://creativecommons.org/publicdomain/zero/1.0/  |
# |                                                      |
# +------------------------------------------------------+

extends Panel

onready var L : Label = $Center/Label
var msg : String = ""

func _ready() -> void:
	msg = L.text
	L.text = ""
	for translation in WishTranslation.translations:
		L.text += "%s\n" % translation.get_message(msg)
	for node in get_tree().get_root().get_children():
		if node == self:
			continue
		node.queue_free()

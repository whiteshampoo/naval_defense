# +------------------------------------------------------+
# |                                                      |
# | Written by Benedikt Wicklein aka whiteshampoo - 2022 | 
# |                                                      |
# |                     License: CC0                     |
# |  https://creativecommons.org/publicdomain/zero/1.0/  |
# |                                                      |
# +------------------------------------------------------+

extends HBoxContainer
class_name WishModSlider
tool

signal value_changed

onready var Name : Label = $Name
onready var Slide : HSlider = $Slide

export var slider_name : String = "ModSlider" setget set_slider_name

export var minimum : float = -1.0
export var zero : float = 0.0
export var maximum : float = 1.0

export(float, EASE) var negative : float = 1.0
export(float, EASE) var positive : float = 1.0

var mod_value : float = 0.0 setget set_mod_value, get_mod_value

func set_slider_name(s : String) -> void:
	slider_name = s
	if is_instance_valid(Name):
		Name.text = slider_name


func set_mod_value(_new_value : float) -> void:
	Slide.value = calc_unmodified_value(_new_value)



func get_mod_value() -> float:
	return mod_value
	

func _ready() -> void:
	set_slider_name(slider_name)


func lerp_ease(_zero : float, _range : float, _value : float, _ease : float) -> float:
	return lerp(_zero, _range, ease(_value, _ease))


func unease_invlerp(_zero : float, _range : float, _value : float, _ease : float) -> float:
	return 1.0 - ease(1.0 - inverse_lerp(_zero, _range, _value), 1.0 / _ease)


func calc_modified_value(value : float) -> float:
	if value > 0.0:
		return lerp_ease(zero, maximum, value, positive)
	if value < 0.0:
		return lerp_ease(zero, minimum, -value, negative)
	return zero


func calc_unmodified_value(value : float) -> float:
	if value > zero:
		return unease_invlerp(zero, maximum, value, positive)
	if value < zero:
		return -unease_invlerp(zero, minimum, value, negative)
	return 0.0


func _on_Slide_value_changed(value: float) -> void:
	mod_value = calc_modified_value(value)
	emit_signal("value_changed", mod_value)


func _on_Default_pressed() -> void:
	Slide.value = 0.0

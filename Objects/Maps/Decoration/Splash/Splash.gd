# +------------------------------------------------------+
# |                                                      |
# | Written by Benedikt Wicklein aka whiteshampoo - 2022 | 
# |                                                      |
# |               License: CC BY-NC-SA 4.0               |
# |  https://creativecommons.org/licenses/by-nc-sa/4.0/  |
# |                                                      |
# +------------------------------------------------------+


extends CPUParticles2D

#-[CONSTANTS]---------------------------------------------------------------------



#-[SIGNALS]---------------------------------------------------------------------



#-[EXPORT]----------------------------------------------------------------------

#export (float, 0.0, 1.0, 0.01) var progress : float = 0.0
#export var circle_color : Color = Color.white
#export (float, 8.0, 64.0, 0.1) var circle_size : float = 16.0
#export (float, 0.1, 1.0, 0.01) var circle_duration : float = 0.5

#-[ONREADY]---------------------------------------------------------------------



#-[VAR]-------------------------------------------------------------------------

var original_position : Vector2 = Vector2.ZERO

#-[SETGET METHODS]--------------------------------------------------------------



#-[BUILTIN GODOT METHODS]-------------------------------------------------------

func _ready() -> void:
	if not original_position:
		original_position = global_position
	emitting = true
	$Splash2.emitting = true
	#set_as_toplevel(true)
	$Audio.pitch_scale = rand_range(0.9, 1.1)
	$Audio.play()


func _process(_delta: float) -> void:
	global_position = original_position


func _physics_process(_delta : float) -> void:
	if not emitting:
		queue_free()
	

#-[INHERITED METHODS]-----------------------------------------------------------



#-[OWN METHODS]-----------------------------------------------------------------



#-[SIGNAL METHODS]--------------------------------------------------------------

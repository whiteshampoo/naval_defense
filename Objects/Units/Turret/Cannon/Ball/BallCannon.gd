# +------------------------------------------------------+
# |                                                      |
# | Written by Benedikt Wicklein aka whiteshampoo - 2022 | 
# |                                                      |
# |               License: CC BY-NC-SA 4.0               |
# |  https://creativecommons.org/licenses/by-nc-sa/4.0/  |
# |                                                      |
# +------------------------------------------------------+


extends Area2D

#-[CONSTANTS]---------------------------------------------------------------------



#-[SIGNALS]---------------------------------------------------------------------



#-[EXPORT]----------------------------------------------------------------------

export var size : float = 16.0
export var damage_body = 1.0
export var damage_sail = 5.0

#-[ONREADY]---------------------------------------------------------------------

onready var Trail : Line2D = $Trail
onready var TB : Tween = $TweenBall
onready var TT : Tween = $TweenTrail
onready var Visual : Sprite = $Visual
onready var Smoke : CPUParticles2D = $Smoke

#-[VAR]-------------------------------------------------------------------------

var targets : Array = Array()
var target_group : String = ""
var hit : bool = false
var avg_speed : float = 0.0

#-[SETGET METHODS]--------------------------------------------------------------



#-[BUILTIN GODOT METHODS]-------------------------------------------------------

func _ready() -> void:
	Trail.set_as_toplevel(true)
	Trail.points[0] = global_position
	Trail.points[1] = global_position


func _process(delta : float) -> void:
	rotation += delta * TAU * 4.0



#-[INHERITED METHODS]-----------------------------------------------------------



#-[OWN METHODS]-----------------------------------------------------------------

func shoot(init_pos : Vector2, target_pos : Vector2, speed : float, _target_group : String) -> void:
	target_group = _target_group
	global_position = init_pos
	look_at(target_pos)
	var duration : float = init_pos.distance_to(target_pos) / (32.0 * speed)
	# warning-ignore:return_value_discarded
	TB.interpolate_property(self, "global_position", init_pos, target_pos, duration)
# warning-ignore:return_value_discarded
	TB.interpolate_method(self, "update_trail_0", init_pos, target_pos, duration, 0, 2)
# warning-ignore:return_value_discarded
	TT.interpolate_method(self, "update_trail_1", init_pos, target_pos, duration, 0, 2, 0.25)
	TB.start() # warning-ignore:return_value_discarded
	TT.start() # warning-ignore:return_value_discarded
	Smoke.emitting = true


func update_trail_0(vec : Vector2) -> void:
	Trail.points[0] = vec


func update_trail_1(vec : Vector2) -> void:
	Trail.points[1] = vec


func boost(b : float) -> void:
	damage_body *= b
	damage_sail *= b

#-[SIGNAL METHODS]--------------------------------------------------------------

func _on_BallTemplate_body_entered(body : Node) -> void:
	if body.is_in_group(target_group) and not body in targets:
		targets.append(body)
		


func _on_BallTemplate_body_exited(body : Node) -> void:
	if body in targets:
		targets.erase(body)


func _on_BallTemplate_area_entered(area: Area2D) -> void:
	_on_BallTemplate_body_entered(area)


func _on_BallTemplate_area_exited(area: Area2D) -> void:
	_on_BallTemplate_body_exited(area)


func _on_TweenBall_tween_all_completed() -> void:
	Visual.visible = false
	if not targets:
		get_tree().get_nodes_in_group("MapSand")[0].add_splash(global_position)
		return
	for hit_target in targets:
		hit_target.damage(damage_body, damage_sail)
	var explosion : CPUParticles2D = preload("res://Objects/Units/Turret/Cannon/Ball/BallExplosion.tscn").instance()
	explosion.global_position = global_position
	get_parent().get_parent().add_child(explosion)


func _on_TweenTrail_tween_all_completed() -> void:
	queue_free()

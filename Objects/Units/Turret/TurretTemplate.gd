# +------------------------------------------------------+
# |                                                      |
# | Written by Benedikt Wicklein aka whiteshampoo - 2022 | 
# |                                                      |
# |               License: CC BY-NC-SA 4.0               |
# |  https://creativecommons.org/licenses/by-nc-sa/4.0/  |
# |                                                      |
# +------------------------------------------------------+


extends Area2D

#-[CONSTANTS]---------------------------------------------------------------------



#-[SIGNALS]---------------------------------------------------------------------



#-[EXPORT]----------------------------------------------------------------------

export var ball_speed : float = 8.0
export var ball_scene : PackedScene = null
export var target_group : String = ""

#-[ONREADY]---------------------------------------------------------------------

onready var BallPosition : Position2D = $Chassis/Barrel/BallPosition
onready var Anim : AnimationPlayer = $Chassis/AnimationPlayer
onready var Cooldown : Timer = $Cooldown
onready var SightRadius : CollisionShape2D = $Sight/Radius

onready var original_sight : float = SightRadius.shape.radius
onready var original_cooldown : float = Cooldown.wait_time

#-[VAR]-------------------------------------------------------------------------

var nodes_in_sight : Array = Array()
var active_target : Node2D = null
var boost : float = 1.0
var can_fire : bool = true

#-[SETGET METHODS]--------------------------------------------------------------



#-[BUILTIN GODOT METHODS]-------------------------------------------------------

func _ready() -> void:
	assert(target_group, "No target-group")
	assert(ball_scene, "No ball-scene")



func _physics_process(_delta : float) -> void:
	if is_instance_valid(active_target):
		look_at(active_target.global_position)
		if Cooldown.is_stopped() and can_fire:
			var target_pos : Vector2 = active_target.global_position + Vector2.UP.rotated(randf() * TAU) * 16.0
			if active_target.is_in_group("Ship"):
				target_pos +=  active_target.direction * active_target.speed / ball_speed
			var ball : Area2D = ball_scene.instance()
			ball.boost(boost)
			get_parent().add_child(ball)
			ball.shoot(BallPosition.global_position,
						target_pos,
						ball_speed,
						target_group)
			Anim.play("Shoot")
			$Boom.pitch_scale = 1.0 + (randf() - 0.5) / 5.0
			$Boom.play()
			Cooldown.start()
	else:
		find_new_target()

#-[INHERITED METHODS]-----------------------------------------------------------



#-[OWN METHODS]-----------------------------------------------------------------

func upgrade(sight : float, speed : float) -> void:
	SightRadius.shape.radius = original_sight * sight
	Cooldown.wait_time =  original_cooldown / speed

func find_new_target() -> void:
	for node in nodes_in_sight.duplicate():
		if not is_instance_valid(node):
			nodes_in_sight.erase(node)
	
	if not nodes_in_sight: return
	
	active_target = nodes_in_sight[0]

#-[SIGNAL METHODS]--------------------------------------------------------------


func _on_Sight_body_entered(body : Node) -> void:
	if body in nodes_in_sight: return
	if not body.is_in_group(target_group): return
	nodes_in_sight.append(body)
	if not active_target:
		active_target = body


func _on_Sight_body_exited(body: Node) -> void:
	if body in nodes_in_sight:
		nodes_in_sight.erase(body)
	if body == active_target:
		active_target = null


func _on_Sight_area_entered(area: Area2D) -> void:
	_on_Sight_body_entered(area)


func _on_Sight_area_exited(area: Area2D) -> void:
	_on_Sight_body_exited(area)

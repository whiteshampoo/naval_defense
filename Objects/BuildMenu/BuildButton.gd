# +------------------------------------------------------+
# |                                                      |
# | Written by Benedikt Wicklein aka whiteshampoo - 2022 | 
# |                                                      |
# |               License: CC BY-NC-SA 4.0               |
# |  https://creativecommons.org/licenses/by-nc-sa/4.0/  |
# |                                                      |
# +------------------------------------------------------+


extends Button

#-[CONSTANTS]---------------------------------------------------------------------



#-[SIGNALS]---------------------------------------------------------------------

signal button_pressed(button_id, minus_gold)

#-[EXPORT]----------------------------------------------------------------------

export var button_id : String
export var cost : int = 0 setget set_cost
export var icon_tex : Texture = null
export var tech_level : int = 0

#-[ONREADY]---------------------------------------------------------------------



#-[VAR]-------------------------------------------------------------------------



#-[SETGET METHODS]--------------------------------------------------------------

func set_cost(_cost : int) -> void:
	cost = _cost
	$Cost.visible = cost != 0
	if cost > 0:
		$Cost.text = tr("§_COST").format({"cost": cost})
	elif cost < 0:
		$Cost.text = tr("§_VALUE").format({"cost": -cost})
	else:
		$Cost.text = ""

#-[BUILTIN GODOT METHODS]-------------------------------------------------------

func _ready() -> void:
	test_tech()
	assert(button_id, "No button_id")
	if icon_tex:
		$Icon.texture = icon_tex
	set_cost(cost)


func _process(_delta : float) -> void:
	pass


func _physics_process(_delta : float) -> void:
	pass



#-[INHERITED METHODS]-----------------------------------------------------------



#-[OWN METHODS]-----------------------------------------------------------------

func test_tech() -> void:
	if not cost and not visible: return
	visible = tech_level <= get_tree().get_nodes_in_group("Level")[0].tech_level

#-[SIGNAL METHODS]--------------------------------------------------------------


func _on_Button_pressed() -> void:
	emit_signal("button_pressed", button_id, cost)
